package org.insa.algo.shortestpath;

import java.util.*;
import org.insa.graph.*;
import org.insa.algo.utils.*;
import org.insa.algo.AbstractSolution.Status;


public class DijkstraAlgorithm extends ShortestPathAlgorithm {

    public DijkstraAlgorithm(ShortestPathData data) {
        super(data);
    }

    @Override
    protected ShortestPathSolution doRun() {
        ShortestPathData data = getInputData();
        
        // INIT
        BinaryHeap<Label> tas = new BinaryHeap<Label>();
        ArrayList<Label> liste_label = new ArrayList<Label>();
        
        // init de notre tableau de label
        for (Node n : data.getGraph())	{
    		liste_label.add(n.getId(), new Label(n, Double.POSITIVE_INFINITY));
        }
        
        //Origine 1er le tas, avec un coût nul
        liste_label.get(data.getOrigin().getId()).setValue(0);
        tas.insert(liste_label.get(data.getOrigin().getId()));

        //Tant que l'arrivée n'est pas atteinte, et que le tas n'est pas vide
        boolean cont = true;
        while(!tas.isEmpty() && cont)	{
        	//Extraction du min et marquage
        	Label x = tas.deleteMin();
        	Node node_x = x.getNode();
        	x.visit();
        	notifyNodeMarked(node_x);
        	
        	//Algorithme en vers 1 vers 1, on s'arrête dès que l'arrivée a été éliminée
        	if(node_x.compareTo(data.getDestination()) == 0)	{
        		cont = false;
        		continue;
        	}
        	
        	for (Arc successor : node_x)	{
        		//Verification que la route est autorisée
    			if (!data.isAllowed(successor)) {
    				continue;
    			}
            	
        		Node node_y = successor.getDestination();
        		Label y = liste_label.get(node_y.getId());
        		//Si le node n'a pas déjà été visité
        		if(!y.getVisit())	{
            		double newWay = data.getCost(successor)+x.getValue();	//Nouveau chemin possible            		
            		//Si le temps de parcours est plus court que celui retenu	
            		if(newWay < y.getValue())	{
            			y.setValue(newWay);	//update du coÃ»t du chemin
            			y.setPrevious(node_x);	//update du node utilisé pour le chemin
        				tas.insert(y);	//Ajout du node dans le tas
            		}
        		}
        	}
        }

        //Si l'arrivée n'a jamais été atteinte, il n'y a pas de chemin
        if (liste_label.get(data.getDestination().getId()).getPrevious() == null) {
        	return new ShortestPathSolution(data, Status.INFEASIBLE);
        }
        //Destination atteinte
		notifyDestinationReached(data.getDestination());
        
        //Création de la liste des nodes solution
        List<Node> nodes = new ArrayList<Node>();
        Label parcours = liste_label.get(data.getDestination().getId());	//Point d'arrivée
        Label origin = liste_label.get(data.getOrigin().getId());			//Point de départ
        //Parcours à l'envers depuis l'arrivée jusqu'à  l'origine
        while(parcours != origin)	{
        	nodes.add(parcours.getNode());
        	parcours = liste_label.get(parcours.getPrevious().getId());	//On remonte la liste
        }
        nodes.add(origin.getNode());
    	Collections.reverse(nodes);		//On remet la liste dans l'ordre
    	
        //Renvoi de la solution considérée comme optimale
        return new ShortestPathSolution(data, Status.OPTIMAL, Path.createFastestPathFromNodes(data.getGraph(), nodes));
    }
}
