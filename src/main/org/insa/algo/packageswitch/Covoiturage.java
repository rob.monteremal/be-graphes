package org.insa.algo.packageswitch;

import java.util.ArrayList;

import org.insa.algo.ArcInspectorFactory;
import org.insa.algo.shortestpath.AStarAlgorithm;
import org.insa.algo.shortestpath.DijkstraAlgorithm;
import org.insa.algo.shortestpath.ShortestPathData;
import org.insa.algo.shortestpath.ShortestPathSolution;
import org.insa.graph.Node;

public class Covoiturage extends AStarAlgorithm
{

	private Node origin2;
	
	public Covoiturage(ShortestPathData data, Node origin2)
	{
		super(data);
		this.origin2 = origin2;
	}
	
	protected ShortestPathSolution doRun()
	{
		// INIT
		long time_start = System.currentTimeMillis();
		Node destination = this.getInputData().getDestination();
		Node origin1 = this.getInputData().getOrigin();
		Node origin2 = this.origin2;
		
		// lancement algo
		
		// calcul préliminaire de Dijkstra de 1 vers n à partir de O1, O2 et D, et 
		// enregistrement dans un tableau a 3 colonnes
		ArrayList<ArrayList<ShortestPathSolution>> tabSolution = new ArrayList<ArrayList<ShortestPathSolution>>();
		
		for (Node n : this.getInputData().getGraph())
		{

			// en partant de origin1
			tabSolution.add(new ArrayList<ShortestPathSolution>());
			tabSolution.get(tabSolution.size()-1).add(new DijkstraAlgorithm(new ShortestPathData(this.getInputData().getGraph(), origin1, n, ArcInspectorFactory.getAllFilters().get(0))).run());
			
			// en partant de origin2
			tabSolution.add(new ArrayList<ShortestPathSolution>());
			tabSolution.get(tabSolution.size()-1).add(new DijkstraAlgorithm(new ShortestPathData(this.getInputData().getGraph(), origin2, n, ArcInspectorFactory.getAllFilters().get(0))).run());
			
			// en partant de destination
			tabSolution.add(new ArrayList<ShortestPathSolution>());
			tabSolution.get(tabSolution.size()-1).add(new DijkstraAlgorithm(new ShortestPathData(this.getInputData().getGraph(), destination, n, ArcInspectorFactory.getAllFilters().get(0))).run());
			
		}
		
		//System.out.println(tabSolution.get(tabSolution.size()-1));
		// Comparaison distance
		for (int k = 0;  k < this.getInputData().getGraph().size(); k++)
		{
			
		}
		
		// TODO: trouver le VRAI nodeSol
		Node nodeSol = this.getInputData().getOrigin();
		
		// solution finale avec point de rendez-vous
		ArrayList<ShortestPathSolution> solutionFinale = new ArrayList<ShortestPathSolution>();
		solutionFinale.add(new DijkstraAlgorithm(new ShortestPathData(this.getInputData().getGraph(), origin1, nodeSol, ArcInspectorFactory.getAllFilters().get(1))).run());
		solutionFinale.add(new DijkstraAlgorithm(new ShortestPathData(this.getInputData().getGraph(), origin2, nodeSol, ArcInspectorFactory.getAllFilters().get(1))).run());
		solutionFinale.add(new DijkstraAlgorithm(new ShortestPathData(this.getInputData().getGraph(), nodeSol, destination, ArcInspectorFactory.getAllFilters().get(0))).run());
		
		// END
		long time_end = System.currentTimeMillis();
		double time_diff = (double)(time_end-time_start)/1000; //Returns the time in seconds in took for the algorithm
		System.out.println(time_diff);
		return null;
	}
	
}