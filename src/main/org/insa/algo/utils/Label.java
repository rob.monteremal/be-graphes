package org.insa.algo.utils;

import org.insa.graph.Node;

public class Label implements Comparable<Label>
{

	//Used for Dijkstra's Algorithm
	private double value;
	private Node node;
	private Node previous;
	private boolean visited = false;

	//Only used in A-Star
	private double distance;
	private boolean hasDistance = false;
	
	public Label(Node n, double value)
	{
		this.node = n;
		this.value = value;
		this.previous = null;		//Unused at first
	}

	//Used in A-Star
	public Label(Node n, double value, double distance)	{
		this(n, value);
		this.hasDistance = true;
		this.distance = distance;
	}
	
	/**
	 * marks the label as visited
	 */
	public void visit()	{
		this.visited = true;
	}
	
	/**
	 * 
	 * @return wether the label has been visited or not
	 */
	public boolean getVisit()	{
		return this.visited;
	}
	
	/**
	 * sets the parent node of this label
	 */
	public void setPrevious(Node p)
	{
		this.previous = p;
	}

	/**
	 * 
	 * @return the previous node of this label
	 */
	public Node getPrevious()
	{
		return this.previous;
	}

	/**
	 * sets the value of this label
	 */
	public void setValue(double v)
	{
		this.value = v;
	}

	/**
	 * 
	 * @return estimated distance from this node to the end
	 */
	public double getDistance()
	{
		return this.distance;
	}
	
	/**
	 * sets the estimated distance of this label
	 */
	public void setDistance(double distance)
	{
		this.distance = distance;
	}

	/**
	 * 
	 * @return the value of this label
	 */
	public double getValue()
	{
		return this.value;
	}
	
	/**
	 * 
	 * @return the node associated to the label
	 */
	public Node getNode()
	{
		return this.node;
	}

	/**
	 * 
	 * @return comparison between label values if Dijkstra, comparaison with total if A-Star
	 */
	public int compareTo(Label l)
	{
		//If A-Star
		if (this.hasDistance)	{
			//If total (value+distance) are the same, prority is given to the one with the lowest distance
			int diff = new Double(this.getValue()+this.getDistance()).compareTo(l.getValue()+l.getDistance());
			if (diff == 0)	{
				return new Double(this.getDistance()).compareTo(l.getDistance());
			} else {
				return diff;
			}
		} else { //If Dijkstra
			return new Double(this.getValue()).compareTo(l.getValue());
		}
	}
}