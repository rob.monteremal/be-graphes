package org.insa.algo.shortestpath;

import java.io.IOException;
import org.junit.Test;

import java.util.Random;
import org.insa.algo.ArcInspectorFactory;

public class testsOnMap {
	Random rand = new Random();
	

	@Test
	//Tests on carre.mapgr
	public void testOnSquare()	{
		try	{
			AbstractTest test = new AbstractTest("maps/carre.mapgr");
			test.loadData(rand.nextInt(test.getNumberOfNodes()), rand.nextInt(test.getNumberOfNodes()), ArcInspectorFactory.getAllFilters().get(0));
			test.testAll();
		} catch (IOException e)	{
			System.out.println(e);
		}
	}
	
	@Test
	//Tests on toulouse.mapgr
	public void testOnToulouse()	{
		try	{
			AbstractTest test = new AbstractTest("maps/toulouse.mapgr");			
			test.loadData(rand.nextInt(test.getNumberOfNodes()), rand.nextInt(test.getNumberOfNodes()), ArcInspectorFactory.getAllFilters().get(0));
			test.testAll();
		} catch (IOException e)	{
			System.out.println(e);
		}
	}
	

	@Test
	//Tests on guadeloupe.mapgr
	public void testOnGuadeloupe()	{
		try	{
			AbstractTest test = new AbstractTest("maps/guadeloupe.mapgr");			
			test.loadData(rand.nextInt(test.getNumberOfNodes()), rand.nextInt(test.getNumberOfNodes()), ArcInspectorFactory.getAllFilters().get(0));
			test.testAll();
		} catch (IOException e)	{
			System.out.println(e);
		}
	}


	@Test
	//Tests on corse.mapgr
	public void testOnCorsica()	{
		try	{
			AbstractTest test = new AbstractTest("maps/corse.mapgr");			
			test.loadData(rand.nextInt(test.getNumberOfNodes()), rand.nextInt(test.getNumberOfNodes()), ArcInspectorFactory.getAllFilters().get(0));
			test.testAll();
		} catch (IOException e)	{
			System.out.println(e);
		}
	}


	@Test
	//Tests on rennes.mapgr
	public void testOnRennes()	{
		try	{
			AbstractTest test = new AbstractTest("maps/rennes.mapgr");			
			test.loadData(rand.nextInt(test.getNumberOfNodes()), rand.nextInt(test.getNumberOfNodes()), ArcInspectorFactory.getAllFilters().get(0));
			test.testAll();
		} catch (IOException e)	{
			System.out.println(e);
		}
	}
}