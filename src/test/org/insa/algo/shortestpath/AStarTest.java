package org.insa.algo.shortestpath;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;

import org.insa.graph.Graph;
import org.insa.graph.io.BinaryGraphReader;
import org.insa.graph.io.GraphReader;
import org.insa.algo.ArcInspector;
import org.insa.algo.ArcInspectorFactory;

public class AStarTest {  
    private static ShortestPathData data;
    
    private static AStarAlgorithm AStar;
    private static ShortestPathSolution pathAStar;
    private static BellmanFordAlgorithm bellman;
    private static ShortestPathSolution pathBellman;
        
    @Test
    public void testMapToulouseAllInspectors()	{
    	String mapName = "maps/toulouse.mapgr";
        // Create a graph reader.
        try {
        	//On teste le chemin pour chaque type d'inspector
        	for (ArcInspector inspector : ArcInspectorFactory.getAllFilters())	{
	        	//Chargement et lecture de la carte
	        	GraphReader reader = new BinaryGraphReader(new DataInputStream(new BufferedInputStream(new FileInputStream(mapName))));
	        	Graph graph = reader.read();
	    	
		        data = new ShortestPathData(graph, graph.get(5912), graph.get(14096), inspector); //chemin INSA-Bikini
		        //AStar
		        AStar = new AStarAlgorithm(data);
		        pathAStar = AStar.run();
		        
		        //Bellman
		        bellman = new BellmanFordAlgorithm(data);
		        pathBellman = bellman.run();

    	    	assertTrue(pathBellman.isFeasible() == pathAStar.isFeasible());
		        if (pathBellman.isFeasible())	{
	    	        assertEquals(pathAStar.getPath().getGraph().size(), pathBellman.getPath().getGraph().size());    	
	    	    	assertEquals(pathAStar.getPath().getLength(), pathBellman.getPath().getLength(), 1e-3);
		        	System.out.println("Longueur: "+pathAStar.getPath().getLength());
		        } else {	//Si chemin non faisable, on ne test pas le path (NPE)
		        	System.out.println("Pas de chemin");
		        }
        	}
        } catch (IOException e)	{
        	System.out.println(e);
        }
    }
}
