package org.insa.algo.shortestpath;

import java.io.IOException;
import org.junit.Test;

import java.util.Random;
import org.insa.algo.ArcInspectorFactory;

public class testsOnMapLoop {
	Random rand = new Random();
	private AbstractTest[] listTests = new AbstractTest[5];
	private String[] listMaps = {"carre", "toulouse", "guadeloupe", "corse", "rennes"};
	int numberOfTests = 100;
	private int numberOfErrors = 0;
	private int numberOfSuccess = 0;
		
	//Loads every map
	private void loadMaps()	{
		for (int i=0 ; i<this.listMaps.length ; i++)	{
			try	{
				this.listTests[i] = new AbstractTest("maps/"+this.listMaps[i]+".mapgr");				
			} catch (IOException e)	{
				System.out.println(e);
			}
		}
	}

	@Test
	//Loops through every map and runs each AbstractTest numberOfTests times
	public void runAllTests()	{
		this.loadMaps();
		for (AbstractTest test : this.listTests)	{
			for (int i=0 ; i<this.numberOfTests ; i++)	{
				this.runTest(test);
			}
		}
		System.out.println(this.numberOfErrors+" tests failed");
		System.out.println(this.numberOfSuccess+" tests passed");
		System.out.println((float)this.numberOfErrors/this.numberOfSuccess*100+" % failure rate");
	}
	
	//Runs a single AbstractTest on a map
	private void runTest(AbstractTest test)	{
		test.loadData(rand.nextInt(test.getNumberOfNodes()), rand.nextInt(test.getNumberOfNodes()), ArcInspectorFactory.getAllFilters().get(0));
		try	{
			test.testAll();
			this.numberOfSuccess++;
		} catch (AssertionError e) {
			this.numberOfErrors++;
			System.out.println("Failure to pass the following test: map: "+test.getMapName()+" start: "+test.getOrigin()+" destination : "+test.getDestination());
		}
	}
}