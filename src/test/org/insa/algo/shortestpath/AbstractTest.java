package org.insa.algo.shortestpath;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;

import org.insa.algo.ArcInspector;
import org.insa.graph.Graph;
import org.insa.graph.io.BinaryGraphReader;
import org.insa.graph.io.GraphReader;


public class AbstractTest {
	private Graph graph;
	private ShortestPathSolution pathBellman;
	private ShortestPathSolution pathDijkstra;
	private ShortestPathSolution pathAStar;
	private ShortestPathData data;
	private int numberOfNodes;
	private int origin;
	private int destination;
	private String mapName;
	private double tolerance = 100; //Acceptable gap between to results to consider them identical (in meters)
	private double toleranceTemps = 100;	//Acceptable gap between to results to consider them identical (in seconds)
	
	//Opens the map and loads the graph
	public AbstractTest(String mapName) throws IOException {
		GraphReader reader = new BinaryGraphReader(new DataInputStream(new BufferedInputStream(new FileInputStream(mapName))));
		this.mapName = mapName;
		this.graph = reader.read();
		this.numberOfNodes = graph.size();
	}
	
	//Loads the necessary data to runs algos
	public void loadData(int IDorigin, int IDdestination, ArcInspector inspector)	{
        this.data = new ShortestPathData(graph, graph.get(IDorigin), graph.get(IDdestination), inspector);
        this.origin = IDorigin;
        this.destination = IDdestination;
        this.runAlgo();
	}
	
	//Runs the 3 algorithms
	private void runAlgo()	{
        this.pathBellman = new BellmanFordAlgorithm(data).run();
        this.pathDijkstra = new DijkstraAlgorithm(data).run();
        this.pathAStar = new AStarAlgorithm(data).run();
	}
	
	//Returns the number of nodes in the graph
	public int getNumberOfNodes()	{
		return this.numberOfNodes;
	}
	
	//Returns the name of the map used currently
	public String getMapName()	{
		return this.mapName;
	}
	
	//Returns the name of the map used currently
	public int getOrigin()	{
		return this.origin;
	}
	
	//Returns the name of the map used currently
	public int getDestination()	{
		return this.destination;
	}
	
	//Runs every test
	public void testAll() throws AssertionError{
		this.testFeasibleDijkstra();
		this.testFeasibleAStar();
		if(pathBellman.isFeasible())	{	//Only tests the paths if they are feasable
			this.testSizeDijkstra();
			this.testLengthDijkstra();
			this.testTimeDijkstra();
			this.testSizeAStar();
			this.testLengthAStar();
			this.testTimeAStar();
		}
	}

	@Test
	//Tests the travel time of the paths
	private void testTimeDijkstra() throws AssertionError{
        assertEquals(pathDijkstra.getPath().getMinimumTravelTime(), pathBellman.getPath().getMinimumTravelTime(), toleranceTemps);
	}

	@Test
	//Tests the length of the paths
	private void testLengthDijkstra() throws AssertionError{
        assertEquals(pathDijkstra.getPath().getLength(), pathBellman.getPath().getLength(), tolerance);
	}

	@Test
	//Tests the number of nodes of the paths
	private void testSizeDijkstra() throws AssertionError{
        assertEquals(pathDijkstra.getPath().getGraph().size(), pathBellman.getPath().getGraph().size());
	}

	@Test
	//Tests if the paths have the same outcome (feasible/unfeasible)
	private void testFeasibleDijkstra() throws AssertionError{
    	assertTrue(pathBellman.isFeasible() == pathDijkstra.isFeasible());
	}

	@Test
	//Tests the travel time of the paths
	private void testTimeAStar() throws AssertionError{
        assertEquals(pathAStar.getPath().getMinimumTravelTime(), pathBellman.getPath().getMinimumTravelTime(), toleranceTemps);		
	}

	@Test
	//Tests the length of the paths
	private void testLengthAStar() throws AssertionError{
        assertEquals(pathAStar.getPath().getLength(), pathBellman.getPath().getLength(), tolerance);
	}

	@Test
	//Tests the number of nodes of the paths
	private void testSizeAStar() throws AssertionError{
        assertEquals(pathAStar.getPath().getGraph().size(), pathBellman.getPath().getGraph().size());
	}

	@Test
	//Tests if the paths have the same outcome (feasible/unfeasible)
	private void testFeasibleAStar() throws AssertionError{
    	assertTrue(pathBellman.isFeasible() == pathAStar.isFeasible());
	}
}