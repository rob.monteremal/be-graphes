package org.insa.algo.shortestpath;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Test;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;

import org.insa.graph.Graph;
import org.insa.graph.Node;
import org.insa.graph.RoadInformation;
import org.insa.graph.RoadInformation.RoadType;
import org.insa.graph.io.BinaryGraphReader;
import org.insa.graph.io.GraphReader;
import org.insa.algo.ArcInspector;
import org.insa.algo.ArcInspectorFactory;

public class DijkstraTest {
    // Small graph use for tests
    private static Graph graph;

    // List of nodes
    private static Node[] nodes;
    
    private static ShortestPathData data;
    private static ArcInspector shortestAll;
    private static RoadInformation info;
    
    private static DijkstraAlgorithm dijkstra;
    private static ShortestPathSolution pathDijkstra;
    private static BellmanFordAlgorithm bellman;
    private static ShortestPathSolution pathBellman;
    
    @BeforeClass
    public static void initAll() {

        // Création du graphe
        nodes = new Node[6];
        for (int i = 0; i < nodes.length; ++i) {
            nodes[i] = new Node(i, null);
        }

        //Les arcs sont à sens unique (arêtes)
        info = new RoadInformation(RoadType.UNCLASSIFIED, null, true, 1, null);
        
        //Creation des arcs entre chaque nodes, avec leur poid associé
        Node.linkNodes(nodes[0], nodes[1], 7, info, null);
        Node.linkNodes(nodes[0], nodes[2], 8, info, null);
        Node.linkNodes(nodes[2], nodes[0], 7, info, null);
        Node.linkNodes(nodes[1], nodes[3], 4, info, null);
        Node.linkNodes(nodes[1], nodes[4], 1, info, null);
        Node.linkNodes(nodes[1], nodes[5], 5, info, null);
        Node.linkNodes(nodes[2], nodes[5], 2, info, null);
        Node.linkNodes(nodes[4], nodes[3], 2, info, null);
        Node.linkNodes(nodes[4], nodes[2], 2, info, null);
        Node.linkNodes(nodes[4], nodes[5], 3, info, null);
        Node.linkNodes(nodes[5], nodes[4], 3, info, null);
        Node.linkNodes(nodes[2], nodes[1], 2, info, null);

        // Création du graph à partir des nodes qui ont été link
        graph = new Graph("ID", "", Arrays.asList(nodes), null);

        // Création des arcs inspector
        shortestAll = ArcInspectorFactory.getAllFilters().get(0);
    }
    
    @Test
    public void testSinglePath()	{
        data = new ShortestPathData(graph, graph.get(1), graph.get(3), shortestAll);
        //Dijkstra
        dijkstra = new DijkstraAlgorithm(data);
        pathDijkstra = dijkstra.run();
        
        //Bellman
        bellman = new BellmanFordAlgorithm(data);
        pathBellman = bellman.run();
    	
    	assertTrue(pathBellman.isFeasible() == pathDijkstra.isFeasible());
        assertEquals(pathDijkstra.getPath().getGraph().size(), pathBellman.getPath().getGraph().size());    	
    	assertEquals(pathDijkstra.getPath().getLength(), pathBellman.getPath().getLength(), 1e-3);
    }
    
    @Test
    public void testEveryPath()	{
    	float solutionTable[][] = new float[nodes.length][nodes.length]; //tableau des longueurs

    	//test des paths entre tous les sommets de départ et arrivée
    	for (int origin = 0 ; origin<nodes.length ; origin++)	{
    		for (int dest = 0 ; dest<nodes.length ; dest++)	{
    	        data = new ShortestPathData(graph, graph.get(origin), graph.get(dest), shortestAll);
    	        //Dijkstra
    	        dijkstra = new DijkstraAlgorithm(data);
    	        pathDijkstra = dijkstra.run();
    	        
    	        //Bellman
    	        bellman = new BellmanFordAlgorithm(data);
    	        pathBellman = bellman.run();

    	    	assertTrue(pathBellman.isFeasible() == pathDijkstra.isFeasible());
    	        if (pathBellman.isFeasible())	{
	    	        assertEquals(pathDijkstra.getPath().getGraph().size(), pathBellman.getPath().getGraph().size());    	
	    	    	assertEquals(pathDijkstra.getPath().getLength(), pathBellman.getPath().getLength(), 1e-3);
	    	    	solutionTable[origin][dest] = pathDijkstra.getPath().getLength();
    	        } else {	//Si chemin non faisable, on ne test pas le path (NPE)
	    	    	solutionTable[origin][dest] = -1;
    	        }
    		}
    	}
    	//Affichage de la table des distances
        for (int i=0 ; i<nodes.length ; i++)	{
        	for (int j=0 ; j<nodes.length ; j++)	{
        		System.out.print(solutionTable[i][j]+"\t");
        	}
    		System.out.println();	//retour à la ligne
        }
    }
    
    @Test
    public void testMapToulouseAllInspectors()	{
    	String mapName = "maps/toulouse.mapgr";
        // Create a graph reader.
        try {
        	//On teste le chemin pour chaque type d'inspector
        	for (ArcInspector inspector : ArcInspectorFactory.getAllFilters())	{
	        	//Chargement et lecture de la carte
	        	GraphReader reader = new BinaryGraphReader(new DataInputStream(new BufferedInputStream(new FileInputStream(mapName))));
	        	Graph graph = reader.read();
	    	
		        data = new ShortestPathData(graph, graph.get(5912), graph.get(14096), inspector); //chemin INSA-Bikini
		        //Dijkstra
		        dijkstra = new DijkstraAlgorithm(data);
		        pathDijkstra = dijkstra.run();
		        
		        //Bellman
		        bellman = new BellmanFordAlgorithm(data);
		        pathBellman = bellman.run();

    	    	assertTrue(pathBellman.isFeasible() == pathDijkstra.isFeasible());
		        if (pathBellman.isFeasible())	{
	    	        assertEquals(pathDijkstra.getPath().getGraph().size(), pathBellman.getPath().getGraph().size());    	
	    	    	assertEquals(pathDijkstra.getPath().getLength(), pathBellman.getPath().getLength(), 1e-3);
		        	System.out.println("Longueur: "+pathDijkstra.getPath().getLength());
		        } else {	//Si chemin non faisable, on ne test pas le path (NPE)
		        	System.out.println("Pas de chemin");
		        }
        	}
        } catch (IOException e)	{
        	System.out.println(e);
        }
    }
}
