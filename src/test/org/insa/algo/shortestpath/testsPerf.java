package org.insa.algo.shortestpath;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.insa.algo.ArcInspector;
import org.insa.algo.ArcInspectorFactory;
import org.insa.graph.Graph;
import org.insa.graph.io.BinaryGraphReader;
import org.insa.graph.io.GraphReader;
import org.junit.Test;

public class testsPerf
{
	static ArrayList<String> lst_maps = new ArrayList<String>();
	static int nb_couple = 100;
	static ArrayList<int[]> lst_couples = new ArrayList<int[]>();
	static GraphReader reader;
	static Graph graph;
	
	static ArrayList<String> t = new ArrayList<String>(Arrays.asList("/home/commetud/3eme Annee MIC/Graphes-et-Algorithmes/Maps/insa.mapgr", "/home/commetud/3eme Annee MIC/Graphes-et-Algorithmes/Maps/toulouse.mapgr"));
	
	
	@Test
	public void perfDijkstraAstar() throws IOException
	{	
		for (String m : t)
		{
			lst_couples = new ArrayList<int[]>();
			
			System.out.println("[Chargement "+m+"]");
			reader = new BinaryGraphReader(new DataInputStream(new BufferedInputStream(new FileInputStream(m))));
			graph = reader.read();
			System.out.println("done");
			
			// creation des couples de points (départ, arrivée)
			System.out.println("choix couple de points ...");
			for (int i = 0; i < nb_couple; i++)
			{
				//System.out.println(Integer.toString(i));
				int pDepart_indice = (int)(Math.random()*(graph.size()));
				int pArrive_indice = (int)(Math.random()*(graph.size()));
				//System.out.println("["+Integer.toString(pDepart_indice)+" | "+Integer.toString(pArrive_indice)+"]");
				if (graph.get(pDepart_indice).getPoint().distanceTo(graph.get(pArrive_indice).getPoint()) < 5000)
				{
					i -= 1;
				}
				else
				{
					int[] e = {pDepart_indice, pArrive_indice};
					lst_couples.add(e);
				}			
			}
			System.out.println("done");
			
			System.out.println("---- [Dijkstra vs AStar] ----\nLancement ...");
			// pour tout scénario possible de parcours
			List<ArcInspector> AllInspector = ArcInspectorFactory.getAllFilters();
			for (ArcInspector i : AllInspector)
			{
				// struct d'un element :
				// [Dijkstra.MinimumTravelTime, Dijkstra.getLength, AStar.getMinimumTravelTime, AStar.getLength]
				ArrayList<ArrayList<Double>> results = new ArrayList<ArrayList<Double>>();
			
				ArrayList<Long> T_Dijkstra = new ArrayList<Long>(); // liste des temps de calculs pour Dijkstra
				ArrayList<Long> T_AStar = new ArrayList<Long>(); // liste des temps de calculs pour AStar
			
				long t_Dijkstra_start = 0;
				long t_Dijkstra_end = 0;
				long t_AStar_start = 0;
				long t_AStar_end = 0;
				long somme_t_Dijkstra = 0;
				long somme_t_AStar = 0;
			
				int compteur_victoire_Dijkstra = 0;
				int compteur_victoire_AStar = 0;
				int compteur_egalite = 0;
				int compteur_erreur = 0;
				
				// pour tout couples, on lance Dijkstra et A*
				int j = 0;
				for (int[] c : lst_couples)
				{
					results.add(new ArrayList<Double>());
					t_Dijkstra_start = System.currentTimeMillis();
					ShortestPathSolution Dijkstra = new DijkstraAlgorithm(new ShortestPathData(graph, graph.get(c[0]), graph.get(c[1]), i)).run();
					t_Dijkstra_end = System.currentTimeMillis();
					if (Dijkstra.isFeasible())
					{
						results.get(results.size()-1).add(Dijkstra.getPath().getMinimumTravelTime());
						results.get(results.size()-1).add((double)Dijkstra.getPath().getLength());
					}
					else
					{
						results.get(results.size()-1).add((double)0);
						results.get(results.size()-1).add((double)0);
					}
				
					t_AStar_start = System.currentTimeMillis();
					ShortestPathSolution AStar = new AStarAlgorithm(new ShortestPathData(graph, graph.get(c[0]), graph.get(c[1]), i)).run();
					t_AStar_end = System.currentTimeMillis();
					if (AStar.isFeasible())
					{
						results.get(results.size()-1).add(AStar.getPath().getMinimumTravelTime());
						results.get(results.size()-1).add((double)AStar.getPath().getLength());
					}
					else
					{
						results.get(results.size()-1).add((double)0);
						results.get(results.size()-1).add((double)0);
					}
					
					// ajout temps de calcul dans listes correspondantes
					T_Dijkstra.add(t_Dijkstra_end - t_Dijkstra_start);
					T_AStar.add(t_AStar_end - t_AStar_start);
					
					// comparaison
					if ((((Math.abs(results.get(results.size()-1).get(0).doubleValue() - results.get(results.size()-1).get(2).doubleValue())/results.get(results.size()-1).get(0).doubleValue() < 0.05) && (Math.abs(results.get(results.size()-1).get(1).doubleValue() - results.get(results.size()-1).get(3).doubleValue())/results.get(results.size()-1).get(1).doubleValue() < 0.05)) || (results.get(results.size()-1).get(0).doubleValue()==0 && results.get(results.size()-1).get(1).doubleValue()==0 && results.get(results.size()-1).get(2).doubleValue() == 0 && results.get(results.size()-1).get(3).doubleValue() == 0)))
					{
						// égalité
						if (Math.abs(T_Dijkstra.get(j) - T_AStar.get(j)) < 0.05*T_Dijkstra.get(j))
						{
							compteur_egalite += 1;
						}
						// AStar
						else if (T_AStar.get(j) < T_Dijkstra.get(j))
						{
							compteur_victoire_AStar += 1;
						}
						// Dijsktra
						else
						{
							compteur_victoire_Dijkstra += 1;
						}
					}
					else
					{
						compteur_erreur += 1;
					}
					
					somme_t_Dijkstra += T_Dijkstra.get(j);
					somme_t_AStar += T_AStar.get(j);
					
					j += 1;
				}
				
				// 	Affichage résultat
				System.out.println("\n[testPerf - "+i.toString()+"]");
				System.out.println("nombre de couple de points (=échantillon) : "+Integer.toString(nb_couple)); 
				System.out.println("nombre de victoire Dijkstra : "+Integer.toString(compteur_victoire_Dijkstra));
				System.out.println("nombre de victoire A* : "+Integer.toString(compteur_victoire_AStar));
				System.out.println("nombre d'égalité : "+Integer.toString(compteur_egalite));
				System.out.println("nombre d'erreur: "+Integer.toString(compteur_erreur));
				System.out.println("Temps de calcul moyen Dijkstra : "+Double.toString((double)somme_t_Dijkstra/(double)T_Dijkstra.size()));
				System.out.println("Temps de calcul moyen A* : "+Double.toString((double)somme_t_AStar/(double)T_AStar.size()));
				System.out.println("Différentiel de temps de calcul entre Dijkstra et A* : "+Double.toString((double)somme_t_Dijkstra/(double)somme_t_AStar)+"\n");
			}		
			System.out.println("---- [END] ----");
		}
	}
	
	
	
	@Test
	public void perfBellmanAStar() throws IOException
	{	
		for (String m : t)
		{
			lst_couples = new ArrayList<int[]>();
			
			System.out.println("[Chargement "+m+"]");
			reader = new BinaryGraphReader(new DataInputStream(new BufferedInputStream(new FileInputStream(m))));
			graph = reader.read();
			System.out.println("done");
			
			// creation des couples de points (départ, arrivée)
			System.out.println("choix couple de points ...");
			for (int i = 0; i < nb_couple; i++)
			{
				//System.out.println(Integer.toString(i));
				int pDepart_indice = (int)(Math.random()*(graph.size()));
				int pArrive_indice = (int)(Math.random()*(graph.size()));
				//System.out.println("["+Integer.toString(pDepart_indice)+" | "+Integer.toString(pArrive_indice)+"]");
				if (graph.get(pDepart_indice).getPoint().distanceTo(graph.get(pArrive_indice).getPoint()) < 5000)
				{
					i -= 1;
				}
				else
				{
					int[] e = {pDepart_indice, pArrive_indice};
					lst_couples.add(e);
				}			
			}
			System.out.println("done");
			
			System.out.println("---- [Bellman-Ford vs A*] ----\nLancement ...");
			// pour tout scénario possible de parcours
			List<ArcInspector> AllInspector = ArcInspectorFactory.getAllFilters();
			for (ArcInspector i : AllInspector)
			{
				// struct d'un element :
				// [Bellman.MinimumTravelTime, Bellman.getLength, AStar.getMinimumTravelTime, AStar.getLength]
				ArrayList<ArrayList<Double>> results = new ArrayList<ArrayList<Double>>();
			
				ArrayList<Long> T_Bellman = new ArrayList<Long>(); // liste des temps de calculs pour Bellman
				ArrayList<Long> T_AStar = new ArrayList<Long>(); // liste des temps de calculs pour AStar
			
				long t_Bellman_start = 0;
				long t_Bellman_end = 0;
				long t_AStar_start = 0;
				long t_AStar_end = 0;
				long somme_t_Bellman = 0;
				long somme_t_AStar = 0;
			
				int compteur_victoire_Bellman = 0;
				int compteur_victoire_AStar = 0;
				int compteur_egalite = 0;
				int compteur_erreur = 0;
				
				// pour tout couples, on lance Bellman et A*
				int j = 0;
				for (int[] c : lst_couples)
				{
					results.add(new ArrayList<Double>());
					t_Bellman_start = System.currentTimeMillis();
					ShortestPathSolution BellmanFord = new BellmanFordAlgorithm(new ShortestPathData(graph, graph.get(c[0]), graph.get(c[1]), i)).run();
					t_Bellman_end = System.currentTimeMillis();
					if (BellmanFord.isFeasible())
					{
						results.get(results.size()-1).add(BellmanFord.getPath().getMinimumTravelTime());
						results.get(results.size()-1).add((double)BellmanFord.getPath().getLength());
					}
					else
					{
						results.get(results.size()-1).add((double)0);
						results.get(results.size()-1).add((double)0);
					}
				
					t_AStar_start = System.currentTimeMillis();
					ShortestPathSolution AStar = new AStarAlgorithm(new ShortestPathData(graph, graph.get(c[0]), graph.get(c[1]), i)).run();
					t_AStar_end = System.currentTimeMillis();
					if (AStar.isFeasible())
					{
						results.get(results.size()-1).add(AStar.getPath().getMinimumTravelTime());
						results.get(results.size()-1).add((double)AStar.getPath().getLength());
					}
					else
					{
						results.get(results.size()-1).add((double)0);
						results.get(results.size()-1).add((double)0);
					}
					
					// ajout temps de calcul dans listes correspondantes
					T_Bellman.add(t_Bellman_end - t_Bellman_start);
					T_AStar.add(t_AStar_end - t_AStar_start);
					
					// comparaison des chemins pris (accepte 5% de différence)
					if ((((Math.abs(results.get(results.size()-1).get(0).doubleValue() - results.get(results.size()-1).get(2).doubleValue())/results.get(results.size()-1).get(0).doubleValue() < 0.05) && (Math.abs(results.get(results.size()-1).get(1).doubleValue() - results.get(results.size()-1).get(3).doubleValue())/results.get(results.size()-1).get(1).doubleValue() < 0.05)) || (results.get(results.size()-1).get(0).doubleValue()==0 && results.get(results.size()-1).get(1).doubleValue()==0 && results.get(results.size()-1).get(2).doubleValue() == 0 && results.get(results.size()-1).get(3).doubleValue() == 0)))
					{
						// égalité
						if (Math.abs(T_Bellman.get(j) - T_AStar.get(j)) < 0.05*T_Bellman.get(j))
						{
							compteur_egalite += 1;
						}
						// AStar
						else if (T_AStar.get(j) < T_Bellman.get(j))
						{
							compteur_victoire_AStar += 1;
						}
						// Bellman
						else
						{
							compteur_victoire_Bellman += 1;
						}
					}
					else
					{
						compteur_erreur += 1;
					}
					
					somme_t_Bellman += T_Bellman.get(j);
					somme_t_AStar += T_AStar.get(j);
					
					j += 1;
				}
				
				// 	Affichage résultat
				System.out.println("\n[testPerf - "+i.toString()+"]");
				System.out.println("nombre de couple de points (=échantillon) : "+Integer.toString(nb_couple)); 
				System.out.println("nombre de victoire Bellman-Ford : "+Integer.toString(compteur_victoire_Bellman));
				System.out.println("nombre de victoire A* : "+Integer.toString(compteur_victoire_AStar));
				System.out.println("nombre d'égalité : "+Integer.toString(compteur_egalite));
				System.out.println("nombre d'erreur: "+Integer.toString(compteur_erreur));
				System.out.println("Temps de calcul moyen Bellman-Ford : "+Double.toString((double)somme_t_Bellman/(double)T_Bellman.size()));
				System.out.println("Temps de calcul moyen A* : "+Double.toString((double)somme_t_AStar/(double)T_AStar.size()));
				System.out.println("Différentiel de temps de calcul entre Bellman-Ford et A* : "+Double.toString((double)somme_t_Bellman/(double)somme_t_AStar)+"\n");
			}		
			System.out.println("---- [END] ----");
		}
	}


	@Test
	public void perfBellmanDijkstra() throws IOException
	{	
		for (String m : t)
		{
			lst_couples = new ArrayList<int[]>();
			
			System.out.println("[Chargement "+m+"]");
			reader = new BinaryGraphReader(new DataInputStream(new BufferedInputStream(new FileInputStream(m))));
			graph = reader.read();
			System.out.println("done");
			
			// creation des couples de points (départ, arrivée)
			System.out.println("choix couple de points ...");
			for (int i = 0; i < nb_couple; i++)
			{
				//System.out.println(Integer.toString(i));
				int pDepart_indice = (int)(Math.random()*(graph.size()));
				int pArrive_indice = (int)(Math.random()*(graph.size()));
				//System.out.println("["+Integer.toString(pDepart_indice)+" | "+Integer.toString(pArrive_indice)+"]");
				if (graph.get(pDepart_indice).getPoint().distanceTo(graph.get(pArrive_indice).getPoint()) < 5000)
				{
					i -= 1;
				}
				else
				{
					int[] e = {pDepart_indice, pArrive_indice};
					lst_couples.add(e);
				}			
			}
			System.out.println("done");
			
			System.out.println("---- [Bellman-Ford vs Dijkstra] ----\nLancement ...");
			// pour tout scénario possible de parcours
			List<ArcInspector> AllInspector = ArcInspectorFactory.getAllFilters();
			for (ArcInspector i : AllInspector)
			{
				// struct d'un element :
				// [Bellman.MinimumTravelTime, Bellman.getLength, Dijkstra.getMinimumTravelTime, Dijkstra.getLength]
				ArrayList<ArrayList<Double>> results = new ArrayList<ArrayList<Double>>();
			
				ArrayList<Long> T_Bellman = new ArrayList<Long>(); // liste des temps de calculs pour Bellman
				ArrayList<Long> T_Dijkstra = new ArrayList<Long>(); // liste des temps de calculs pour Dijkstra
			
				long t_Bellman_start = 0;
				long t_Bellman_end = 0;
				long t_Dijkstra_start = 0;
				long t_Dijkstra_end = 0;
				long somme_t_Bellman = 0;
				long somme_t_Dijkstra = 0;
			
				int compteur_victoire_Bellman = 0;
				int compteur_victoire_Dijkstra = 0;
				int compteur_egalite = 0;
				int compteur_erreur = 0;
				
				// pour tout couples, on lance Bellman et Dijkstra
				int j = 0;
				for (int[] c : lst_couples)
				{
					results.add(new ArrayList<Double>());
					t_Bellman_start = System.currentTimeMillis();
					ShortestPathSolution BellmanFord = new BellmanFordAlgorithm(new ShortestPathData(graph, graph.get(c[0]), graph.get(c[1]), i)).run();
					t_Bellman_end = System.currentTimeMillis();
					if (BellmanFord.isFeasible())
					{
						results.get(results.size()-1).add(BellmanFord.getPath().getMinimumTravelTime());
						results.get(results.size()-1).add((double)BellmanFord.getPath().getLength());
					}
					else
					{
						results.get(results.size()-1).add((double)0);
						results.get(results.size()-1).add((double)0);
					}
				
					t_Dijkstra_start = System.currentTimeMillis();
					ShortestPathSolution Dijkstra = new DijkstraAlgorithm(new ShortestPathData(graph, graph.get(c[0]), graph.get(c[1]), i)).run();
					t_Dijkstra_end = System.currentTimeMillis();
					if (Dijkstra.isFeasible())
					{
						results.get(results.size()-1).add(Dijkstra.getPath().getMinimumTravelTime());
						results.get(results.size()-1).add((double)Dijkstra.getPath().getLength());
					}
					else
					{
						results.get(results.size()-1).add((double)0);
						results.get(results.size()-1).add((double)0);
					}
					
					// ajout temps de calcul dans listes correspondantes
					T_Bellman.add(t_Bellman_end - t_Bellman_start);
					T_Dijkstra.add(t_Dijkstra_end - t_Dijkstra_start);
					
					// comparaison
					if ((((Math.abs(results.get(results.size()-1).get(0).doubleValue() - results.get(results.size()-1).get(2).doubleValue())/results.get(results.size()-1).get(0).doubleValue() < 0.05) && (Math.abs(results.get(results.size()-1).get(1).doubleValue() - results.get(results.size()-1).get(3).doubleValue())/results.get(results.size()-1).get(1).doubleValue() < 0.05)) || (results.get(results.size()-1).get(0).doubleValue()==0 && results.get(results.size()-1).get(1).doubleValue()==0 && results.get(results.size()-1).get(2).doubleValue() == 0 && results.get(results.size()-1).get(3).doubleValue() == 0)))
					{
						// égalité
						if (Math.abs(T_Bellman.get(j) - T_Dijkstra.get(j)) < 0.05*T_Bellman.get(j))
						{
							compteur_egalite += 1;
						}
						// AStar
						else if (T_Dijkstra.get(j) < T_Bellman.get(j))
						{
							compteur_victoire_Dijkstra += 1;
						}
						// Bellman
						else
						{
							compteur_victoire_Bellman += 1;
						}
					}
					else
					{
						compteur_erreur += 1;
					}
					
					somme_t_Bellman += T_Bellman.get(j);
					somme_t_Dijkstra += T_Dijkstra.get(j);
					
					j += 1;
				}
				
				// 	Affichage résultat
				System.out.println("\n[testPerf - "+i.toString()+"]");
				System.out.println("nombre de couple de points (=échantillon) : "+Integer.toString(nb_couple)); 
				System.out.println("nombre de victoire Bellman-Ford : "+Integer.toString(compteur_victoire_Bellman));
				System.out.println("nombre de victoire Dijkstra : "+Integer.toString(compteur_victoire_Dijkstra));
				System.out.println("nombre d'égalité : "+Integer.toString(compteur_egalite));
				System.out.println("nombre d'erreur: "+Integer.toString(compteur_erreur));
				System.out.println("Temps de calcul moyen Bellman-Ford : "+Double.toString((double)somme_t_Bellman/(double)T_Bellman.size()));
				System.out.println("Temps de calcul moyen Dijkstra : "+Double.toString((double)somme_t_Dijkstra/(double)T_Dijkstra.size()));
				System.out.println("Différentiel de temps de calcul entre Bellman-Ford et Dijkstra : "+Double.toString((double)somme_t_Bellman/(double)somme_t_Dijkstra)+"\n");
			}		
			System.out.println("---- [END] ----");
		}
	}
}