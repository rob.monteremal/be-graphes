package org.insa.algo.shortestpath;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.insa.graph.Graph;
import org.insa.graph.io.BinaryGraphReader;
import org.insa.graph.io.GraphReader;
import org.junit.Test;

/*
 * Ces tests ne se concentrent que sur A* et Dijkstra, et jouent un peu sur les divers paramètres
 * qu'on retrouve dans les tests de performances incluant Bellman : nombres de couples de points,
 * distance entre les points, taille de la carte ...
*/

class tests_Perf_onlyDA
{
	// listes de toutes les maps utilisées par les tests
	static ArrayList<String> lst_maps = new ArrayList<String>(Arrays.asList("/home/commetud/3eme Annee MIC/Graphes-et-Algorithmes/Maps/insa.mapgr", 
			"/home/commetud/3eme Annee MIC/Graphes-et-Algorithmes/Maps/toulouse.mapgr",
			"/home/commetud/3eme Annee MIC/Graphes-et-Algorithmes/Maps/carre.mapgr",
			"/home/commetud/3eme Annee MIC/Graphes-et-Algorithmes/Maps/carre-dense.mapgr",
			"/home/commetud/3eme Aneee MIC/Graphes-et-Algorithmes/Maps/fractal.mapgr",
			"/home/commetud/3eme Annee MIC/Graphes-et-Algorithmes/Maps/bordeaux.mapgr",
			"/home/commetud/3eme Annee MIC/Graphes-et-Algorithmes/Maps/madagascar.mapgr"));
	
	// nombres couples de points, distance min les séparants et la liste les contenant  tous
	static int nb_couple;
	static int dist_min;
	static ArrayList<int[]> lst_couples;
	

	static GraphReader reader;
	static Graph graph;
	
	@Test
	public void DijkstraAStar() throws IOException
	{
		for (String m : lst_maps)
		{
			lst_couples = new ArrayList<int[]>();
			
			System.out.println("-------- [TESTS SUR "+m+"] --------");
			reader = new BinaryGraphReader(new DataInputStream(new BufferedInputStream(new FileInputStream(m))));
			graph = reader.read();
			
			// creation des couples de points (départ, arrivée)
			for (int i = 1; i < 5; i++)
			{
				for (int j = 1; j < 6; j++)
				{
					nb_couple = i*100;
					dist_min = j*1000;
					System.out.println("[choix de "+Integer.toString(nb_couple)+"couples de points, séparés d'au minimum "+Integer.toString(dist_min)+"m");
					
					for (int k = 0; k < nb_couple; k++)
					{
						//System.out.println(Integer.toString(i));
						int pDepart_indice = (int)(Math.random()*(graph.size()));
						int pArrive_indice = (int)(Math.random()*(graph.size()));
						//System.out.println("["+Integer.toString(pDepart_indice)+" | "+Integer.toString(pArrive_indice)+"]");
						if (graph.get(pDepart_indice).getPoint().distanceTo(graph.get(pArrive_indice).getPoint()) < dist_min)
						{
							k -= 1;
						}
						else
						{
							int[] e = {pDepart_indice, pArrive_indice};
							lst_couples.add(e);
						}
					}
					
					System.out.println("done");
				}
			}
		}
	}
	
}